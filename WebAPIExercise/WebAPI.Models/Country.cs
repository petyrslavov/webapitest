﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebAPI.Models
{
    [Table("country")]
    public class Country: BaseEntity
    {
        public string Alpha2Code { get; set; }
        public string Capital { get; set; }
    }
}
