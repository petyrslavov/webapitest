﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebAPI.Models
{
    [Table("exception_massage")]
    public class ExceptionMessage
    {
        public int Id { get; set; }
        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string Type { get; set; }
    }
}
