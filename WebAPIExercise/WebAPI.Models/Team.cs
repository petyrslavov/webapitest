﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebAPI.Models
{
    [Table("team")]
    public class Team
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Timestamp]
        public byte[] Version { get; set; }

        public int CountryId { get; set; }
        public Country Country { get; set; }
    }
}
