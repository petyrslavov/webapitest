﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebAPI.Models
{
    [Table("request")]
    public class Request
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string Path { get; set; }
        public string Method { get; set; }
        public string UserAgent { get; set; }
        public DateTime RequestedOn { get; set; }
    }
}
