﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.App.Middlewares;

namespace WebAPI.App.Extensions
{
    public static class ExceptionLoggerMiddlewareExtension
    {
        public static IApplicationBuilder UseExceptionLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionLogger>();
        }
    }
}
