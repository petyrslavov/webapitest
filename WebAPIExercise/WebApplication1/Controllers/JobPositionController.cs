﻿using System.Collections;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobPositionController : BaseCRUDController<JobPosition>
    {
        public JobPositionController(ApiDbContext context) : base(context) { }
    }
}