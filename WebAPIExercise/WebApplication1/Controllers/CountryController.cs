﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using WebAPI.App.Options;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : BaseCRUDController<Country>
    {
        private readonly ApiDbContext context;
        private readonly IHttpClientFactory clientFactory;
        private readonly Settings settings;

        public CountryController(
            ApiDbContext context,
            IOptions<Settings> settings,
            IHttpClientFactory clientFactory
            )
            : base(context) {
            this.context = context;
            this.clientFactory = clientFactory;
            this.settings = settings.Value;
        }

        [HttpPost("{name}")]
        public async Task<ActionResult<Country>> GetCountry([FromRoute] string name)
        {
            var countryJsonList = new List<Country>();

            var httpClient = clientFactory.CreateClient();

            using (httpClient)
            {
                using (var response = await httpClient.GetAsync(this.settings.UrlString + $"{name}?fullText = true"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    countryJsonList = JsonConvert.DeserializeObject<List<Country>>(apiResponse);
                }
            }

            var country = countryJsonList[0];

            var countryFromDb = await this.context.Countries.SingleOrDefaultAsync(x => x.Name == name);

            if (countryFromDb == null)
            {
                await this.context.Countries.AddAsync(country);
                await this.context.SaveChangesAsync();
            }
            else
            {
                this.context.Countries.Update(country);
                await this.context.SaveChangesAsync();
            }

            return country;
        }
    }
}