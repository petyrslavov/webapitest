﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class BaseCRUDController<T> : ControllerBase
        where T: class
    {
        private readonly ApiDbContext context;

        public BaseCRUDController(ApiDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IEnumerable> GetObjList() 
        {
            return await this.context.Set<T>().ToListAsync();
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<T>> GetObj([FromRoute] int id)
        {
            var obj = await this.context.Set<T>().FindAsync(id);

            if (obj == null)
            {
                return this.NotFound();
            }

            return obj;
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] T obj)
        {
            this.context.Set<T>().Add(obj);
            await this.context.SaveChangesAsync();

            return this.Ok();
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] T obj)
        {
            this.context.Entry(obj).State = EntityState.Modified;
            await this.context.SaveChangesAsync();

            return NoContent();
        }

        [HttpPost("{id:int}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            var obj = await this.context.Set<T>().FindAsync(id);

            if (obj == null)
            {
                return this.NotFound();
            }

            this.context.Set<T>().Remove(obj);
            await this.context.SaveChangesAsync();

            return this.NoContent();
        }
    }
}