﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using WebAPI.App.Infrastructure;

namespace WebAPI.App.Middlewares
{
    public class ExceptionLogger
    {
        private readonly RequestDelegate next;
        private readonly ILogger logger;

        public ExceptionLogger(RequestDelegate next, ILogger logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await next(httpContext);
            }
            catch (Exception ex)
            {
                await logger.LogException(ex);
                throw;
            }

        }
    }
}
