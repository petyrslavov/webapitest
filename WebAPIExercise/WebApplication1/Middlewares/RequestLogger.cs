﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using WebAPI.App.Infrastructure;

namespace WebAPI.App.Middlewares
{
    public class RequestLogger
    {
        private readonly RequestDelegate next;
        private readonly ILogger logger;

        public RequestLogger(RequestDelegate next, ILogger logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            await logger.LogRequest(httpContext);

            await next(httpContext);
        }
    }
}
