﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.App.Infrastructure
{
    public class LoggerService : ILogger
    {
        private readonly ApiDbContext context;

        public LoggerService(ApiDbContext context)
        {
            this.context = context;
        }

        public async Task LogException(Exception ex)
        {
            ExceptionMessage exception = new ExceptionMessage()
            {
                Message = ex.Message,
                StackTrace = ex.StackTrace,
                Type = ex.GetType().ToString(),
            };

            await context.Exceptions.AddAsync(exception);
            await context.SaveChangesAsync();
        }

        public async Task LogRequest(HttpContext httpContext)
        {
            Request request = new Request()
            {
                IpAddress = httpContext.Connection.RemoteIpAddress.ToString(),
                Method = httpContext.Request.Method,
                Path = httpContext.Request.Path,
                UserAgent = httpContext.Request.Headers["User-Agent"].ToString(),
                RequestedOn = DateTime.Now,
            };
            await context.Requests.AddAsync(request);
            await context.SaveChangesAsync();
        }

    }
}
