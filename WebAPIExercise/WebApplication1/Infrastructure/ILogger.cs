﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Data;

namespace WebAPI.App.Infrastructure
{
    public interface ILogger
    {
        Task LogRequest(HttpContext httpContext);

        Task LogException(Exception ex);
    }
}
