﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebAPI.App.Middlewares;
using WebAPI.App.Infrastructure;
using WebAPI.App.Options;
using WebAPI.Data;
using WebAPI.App.Extensions;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApiDbContext>(opt =>
                opt.UseNpgsql(this.Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Singleton);

            services.AddTransient<ILogger, LoggerService>();
            services.AddHttpClient();


            services.Configure<Settings>(options => Configuration.GetSection("Settings").Bind(options));


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseRequestLogging();
            app.UseExceptionLogging();

            app.UseHttpsRedirection();
            app.UseMvc();

        }
    }
}
