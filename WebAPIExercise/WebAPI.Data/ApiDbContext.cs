﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WebAPI.Models;


namespace WebAPI.Data
{
    public class ApiDbContext: DbContext
    {
        public DbSet<Team> Teams { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<JobPosition> JobPositions { get; set; }

        public DbSet<Request> Requests { get; set; }

        public DbSet<ExceptionMessage> Exceptions { get; set; }

        public ApiDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>()
                .Property(x => x.Version).IsConcurrencyToken();

            modelBuilder.Entity<JobPosition>()
               .Property(x => x.Version).IsConcurrencyToken();

        }
    }
}
